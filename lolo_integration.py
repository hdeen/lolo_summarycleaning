#!/usr/local/bin/python
# coding: utf-8

# from datalab.context import Context
# import datalab.bigquery as bq
# import datalab.storage as storage
import pandas as pd
from StringIO import StringIO


# ### Read in csv filenames 

# In[148]: [ THIS 'STORAGE' WON'T WORK WITHOUT import datalab.storage as storage]

get_ipython().run_cell_magic('storage', 'read --object gs://lolo-destination-bucket-16/lolo-filenames.csv --variable filenames', '')


# ### convert to pandas dataframe and then to list

df = pd.read_csv(StringIO(filenames))
fnames = df[df.columns[0]].tolist()


# ### create base 'master'dataframe to concatenate to

get_ipython().run_cell_magic('storage', 'read --object $fnames[0] --variable sdu_data', '')
master_df = pd.read_csv(StringIO(sdu_data))

# ### loop through filenames, read in, convert to dataframes and concatenate

for file in fnames:
  
  get_ipython().magic('storage', 'read --object $file --variable filedata', '')
  
  df = pd.read_csv(StringIO(filedata))
  
  master_df = pd.concat([master_df, df], axis=0)


# In[176]:

# CLEANING

print 'master dataframe shape : ' + str(master_df.shape) + '\n'
# Below not specific enough?
# columns_to_drop = master_df.columns.tolist()[:20]


# In[195]:

cols_to_drop = [col for col in master_df.columns if 'opensearch' in col]
cols_2_drop = [col for col in master_df.columns if 'feed' in col]
cols_to_drop.extend(cols_2_drop)


# In[199]:

dropped_df = dropped_df.drop(cols_to_drop, 1)


# In[200]:

renamed_df = dropped_df.rename(columns={'category [term]': 'category_term', 
                        'id.1': 'id_1',
                        'link [href].1': 'link_href_1', 
                        'link [hreflang]': 'link_hreflang', 
                        'link [rel].1': 'link_rel_1',
                        'link [type]' : 'link_type',
                        'relevance:score' : 'relevance_score',
                        'scwsatom:domain' : 'scwsatom_domain',
                        'scwsatom:jcid' : 'scwsatom_jcid',
                        'summary [type]' : 'summary_type',
                        'title [type].1' : 'title_type_1',
                        'title.1' : 'title_1',
                        'updated.1' : 'updated_1',
                        'link [rel]' : 'link_rel',
                        'rights [type]' : 'rights_type',
                        'subtitle [type]' : 'subtitle_type',
                        'link [href]':'link_href'
                       })

